﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowCamera : MonoBehaviour
{
	[Range(0.01f, 1f)]
	public float camSpeed = 0.5f;
	[SerializeField]
	public Transform target;

	[SerializeField]
	bool isLookAtPlayer;

	Vector3 cameraOffset;

	void Start()
	{
		cameraOffset = transform.position - target.position;
	}

	void LateUpdate()
	{
		Vector3 toPos = target.position + cameraOffset;
		
		transform.position = Vector3.Slerp(transform.position, toPos, camSpeed);

		if(isLookAtPlayer)
		{
			transform.LookAt(target);
		}
	}
}
